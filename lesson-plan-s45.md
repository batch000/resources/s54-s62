# WDC028 - S45 - React.js - Component-Driven Development

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/161oHaMoQU8_snu-D1d5q5LNXnE-H51huVJcGtBGoghQ/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/161oHaMoQU8_snu-D1d5q5LNXnE-H51huVJcGtBGoghQ/edit#slide=id.g53aad6d9a4_0_745) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/s45-s50) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/fullstack/s45-s50/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                            | Link                                                         |
| -------------------------------- | ------------------------------------------------------------ |
| Create a New React App           | [Link](https://reactjs.org/docs/create-a-new-react-app.html) |
| Virtual DOM                      | [Link](https://www.youtube.com/watch?v=M-Aw4p0pWwg)          |
| React JS Documentation           | [Link](https://reactjs.org/docs/getting-started.html)        |
| Introducing JSX                  | [Link](https://reactjs.org/docs/introducing-jsx.html)        |
| React-Bootstrap Documentation    | [Link](https://react-bootstrap.github.io/)                   |
| Adding Bootstrap                 | [Link](https://create-react-app.dev/docs/adding-bootstrap/)  |
| JavaScript import Statement      | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import) |
| React JS Components and Props    | [Link](https://reactjs.org/docs/components-and-props.html)   |
| JavaScript export Statement      | [Link](https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export) |
| Mount vs Render                  | [Link](https://reacttraining.com/blog/mount-vs-render/)      |
| React-Bootstrap Navbar Component | [Link](https://react-bootstrap.github.io/components/navbar/) |
| React-Bootstrap Nav Component    | [Link](https://react-bootstrap.github.io/components/navs/)   |
| React JS Strict Mode             | [Link](https://reactjs.org/docs/strict-mode.html)            |
| React JS Fragment Component      | [Link](https://reactjs.org/docs/fragments.html)              |
| React-Bootstrap Card Component   | [Link](https://react-bootstrap.github.io/components/cards/)  |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[15 mins] - Creating a React JS Application
	2.[10 mins] - Installing the Babel Linting
	3.[30 mins] - Virtual DOM
	4.[30 mins] - Introducing JSX
	5.[5 mins] - Installing packages
		- React-Bootstrap
		- Bootstrap
	6.[15 mins] - Components
	7.[1 hr 15 mins] - React-Bootstrap Components
		- Navbar
		- Nav
		- Card
	8.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
# Session Objectives

At the end of the session, the students are expected to:

- manage app state by using a React.js Context object within the app.

# Resources

## Instructional Materials

- GitLab Repository
- Google Slide Presentation

## Supplemental Materials

- [Context (React.js Docs)](https://reactjs.org/docs/context.html)

# Lesson Proper

So far we have learned how to manage state within individual components. However, it is sometimes required to handle the state of the whole application.

Context provides a way to share values like these between components without having to explicitly pass a prop through each component.

# Code Discussion

## Creating and Applying the UserContext

Create a file named **UserContext.js** in the **src** folder and add the following code:

```jsx
// Base imports.
import React from 'react';

export default React.createContext();
```

Then, in the **src/App.js**, add the following code:

```jsx
// Base Imports
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App Imports
import UserContext from 'UserContext';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';
import Register from 'pages/Register';
import Login from 'pages/Login';
import NotFound from 'pages/NotFound';

export default function App() {
    const [user, setUser] = useState(null);

    return (
        <UserContext.Provider value={{ user, setUser }}>
            <BrowserRouter>
                <AppNavbar/>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/courses" component={Courses}/>
                    <Route exact path="/register" component={Register}/>
                    <Route exact path="/login" component={Login}/>
                    <Route component={NotFound}/>
                </Switch>
            </BrowserRouter>
        </UserContext.Provider>
    )
}
```

Also, remove the user props sent to the AppNavbar component.

By using the built-in Provider component from the created UserContext, the given values (user and setUser) can be used anywhere in the child components without explicitly passing the values as a prop.

## Using the UserContext in Login and AppNavbar

To use the context provided by the App component, we will need to use the useContext hook. Go to the **src/pages/Login.js** and add the following code:

```jsx
// Base Imports
import React, { useState, useContext } from 'react';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { user, setUser } = useContext(UserContext);

    function login(e) {
        e.preventDefault();
        alert('You are now logged in.');

        setUser({ email: email });

        setEmail('');
        setPassword('');
    }    

    return (
        <Container fluid>
            <h3>Login</h3>
            <Form onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Button variant="success" type="submit">Login</Button>
            </Form>
        </Container>
    )
}
```

Once the login form is submitted, the email will be given as a value to the user context.

To make the AppNavbar respond to the changes in the user context, we must add the following code to **src/components/AppNavbar.js** file:

```jsx
// Base Imports
import React, { Fragment, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

export default function AppNavbar() {
    const { user } = useContext(UserContext);

    let rightNav = (user === null) ? (
        <Fragment>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
        </Fragment>
    ) : (
        <Fragment>
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
        </Fragment>
    );

    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                </Nav>
                <Nav className="ml-auto">
                    {rightNav}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
```

Remove the `{user}` parameter received by the AppNavbar component.

The result should be like this:

![readme-images/s49-login.gif](readme-images/s49-login.gif)

However, we are still in the Login page and when we refresh the page, the context gets reset.

## Saving Context Data in localStorage

Update the following codes so that the context data will be saved even after a hard page reload.

**src/pages/Login.js**

```jsx
export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { user, setUser } = useContext(UserContext);

    function login(e) {
        e.preventDefault();
        alert('You are now logged in.');

        localStorage.setItem('email', email);
        setUser({ email: email });

        setEmail('');
        setPassword('');
    }    

    return (...)
}
```

**src/App.js**

```jsx
export default function App() {
    const [user, setUser] = useState({ email: localStorage.getItem('email') });

    return (...)
}
```

**src/components/AppNavbar.js**

```jsx
export default function AppNavbar() {
    const { user } = useContext(UserContext);

    let rightNav = (user.email === null) ? (
        <Fragment>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
        </Fragment>
    ) : (
        <Fragment>
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
        </Fragment>
    );

    return (...)
}
```

Now, after demonstrating the login again, the context will be saved even after a hard reload and the Logout link will still be shown.

Also, you can look at the current contents of the localStorage by opening DevTools and going to the Application tab.

![readme-images/Untitled.png](readme-images/Untitled.png)

## Redirect After Login

Let's update again the Login component to make it redirect to the homepage when it is detected that there is a user currently logged in.

```jsx
// Base Imports
import React, { useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { user, setUser } = useContext(UserContext);

    function login(e) {
        e.preventDefault();
        alert('You are now logged in.');

        localStorage.setItem('email', email);
        setUser({ email: email });

        setEmail('');
        setPassword('');
    }

    if (user.email !== null) {
        return <Redirect to="/"/>;
    }

    return (...)
}
```

If the `user.email` has a value, instead of showing the Login component, go to the route indicated in the `to` prop of Redirect component.

## Logging Out

Finally, to log out, let's update the **src/App.js** file first.

```jsx
// Base Imports
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App Imports
import UserContext from 'UserContext';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';
import Register from 'pages/Register';
import Login from 'pages/Login';
import NotFound from 'pages/NotFound';

export default function App() {
    const [user, setUser] = useState({ email: localStorage.getItem('email') });

    const unsetUser = () => {
        localStorage.clear();
        setUser({ email: null });
    }

    return (
        <UserContext.Provider value={{ user, setUser, unsetUser }}>
            <BrowserRouter>
                <AppNavbar user={user}/>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/courses" component={Courses}/>
                    <Route exact path="/register" component={Register}/>
                    <Route exact path="/login" component={Login}/>
                    <Route component={NotFound}/>
                </Switch>
            </BrowserRouter>
        </UserContext.Provider>
    )
}
```

Then, add the following code in **src/components/AppNavbar.js**.

```jsx
// Base Imports
import React, { Fragment, useContext } from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

export default function AppNavbar() {
    const { user, unsetUser } = useContext(UserContext);
    const history = useHistory();

    const logout = () => {
        unsetUser();
        history.push('/login');
    }

    let rightNav = (user.email === null) ? (
        <Fragment>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
        </Fragment>
    ) : (
        <Fragment>
            <Nav.Link onClick={logout}>Logout</Nav.Link>
        </Fragment>
    );

    return (...)
}
```

Now, when the Logout link is clicked, we will be redirected to the Login route.

# Activity

## Instructions

If a user is logged in, make the Register page redirect to the Home page.

## Expected Output

See the Solution section for the output.

## Solution

```jsx
// Base Imports
import React, { useState, useEffect, useContext } from 'react';
import { Redirect } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Register() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);
    const { user } = useContext(UserContext);

    ...

    if (user.email !== null) {
        return <Redirect to="/"/>;
    }

    return (...)
}
```
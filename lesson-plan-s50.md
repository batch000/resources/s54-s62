# WDC028 - S50 - React.js - API Integration with Fetch

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1GQSHrWZ6ViBKxJJhpfxNBlPNfaz1yE3wEIm6fwf-5qE/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1GQSHrWZ6ViBKxJJhpfxNBlPNfaz1yE3wEIm6fwf-5qE/edit#slide=id.g53aad6d9a4_0_745) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/s45-s50) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/fullstack/s45-s50/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                            | Link                                                         |
| -------------------------------- | ------------------------------------------------------------ |
| JavaScript fetch API             | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) |
| JavaScript fetch Method          | [Link](https://developer.mozilla.org/en-US/docs/Web/API/fetch) |
| Bearer Token                     | [Link](https://oauth.net/2/bearer-tokens/)                   |
| Sweet Alert 2 Package            | [Link](https://sweetalert2.github.io/)                       |
| React Bootstrap Grid System      | [Link](https://react-bootstrap.github.io/layout/grid/)       |
| React Router DOM Route Component | [Link](https://reactrouter.com/web/api/Route)                |
| React Router DOM useParams Hook  | [Link](https://reactrouter.com/web/api/Hooks/useparams)      |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[45 mins] - JavaScript Fetch API
	2.[30 mins] - Bearer Tokens
	3.[10 mins] - Sweet Alert 2 Package
	4.[15 mins] - Bootstrap Grid System
	5.[40 mins] - React Router DOM Route Component
	6.[40 mins] - React Router DOM useParams Hook
	7.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
# WDC028 - S46 - React.js - Props and States

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1m4azBPCeAk7WkzI109zVIsfMoCbTzgqtZn3RNryLm6c/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1m4azBPCeAk7WkzI109zVIsfMoCbTzgqtZn3RNryLm6c/edit#slide=id.g53aad6d9a4_0_745) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/s45-s50) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/fullstack/s45-s50/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                                        | Link                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| React JS Props                                               | [Link](https://reactjs.org/docs/components-and-props.html)   |
| Props Drilling                                               | [Link](https://medium.com/front-end-weekly/props-drilling-in-react-js-723be80a08e5) |
| Lists and Keys                                               | [Link](https://reactjs.org/docs/lists-and-keys.html)         |
| Typechecking With PropTypes                                  | [Link](https://reactjs.org/docs/typechecking-with-proptypes.html) |
| React JS States                                              | [Link](https://reactjs.org/docs/hooks-state.html;)           |
| Understanding Synchronous and Asynchronous Code in Javascript | [Link](https://blog.devgenius.io/understanding-synchronous-and-asynchronous-code-in-javascript-46734bf29914) |
| Is JavaScript Synchronous or Asynchronous                    | [Link](https://betterprogramming.pub/is-javascript-synchronous-or-asynchronous-what-the-hell-is-a-promise-7aa9dd8f3bfb) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[30 mins] - React JS Props
	2.[30 mins] - Props Drilling
	3.[30 mins] - Creating components using the map method
	4.[15 mins] - Typechecking with PropTypes
	5.[45 mins] - React JS States
	6.[30 mins] - Synchronous and Asynchronous code in JavaScript
	7.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)

